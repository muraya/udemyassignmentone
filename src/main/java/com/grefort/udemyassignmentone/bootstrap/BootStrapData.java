package com.grefort.udemyassignmentone.bootstrap;

import com.grefort.udemyassignmentone.model.Author;
import com.grefort.udemyassignmentone.model.Book;
import com.grefort.udemyassignmentone.model.Publisher;
import com.grefort.udemyassignmentone.repositories.AuthorRespository;
import com.grefort.udemyassignmentone.repositories.BookRepository;
import com.grefort.udemyassignmentone.repositories.PublisherRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Set;

@Component
public class BootStrapData implements CommandLineRunner {

    private final AuthorRespository authorRespository;
    private final BookRepository bookRepository;
    private final PublisherRepository publisherRepository;

    public BootStrapData(AuthorRespository authorRespository, BookRepository bookRepository, PublisherRepository publisherRepository) {
        this.authorRespository = authorRespository;
        this.bookRepository = bookRepository;
        this.publisherRepository = publisherRepository;
    }

    @Override
    public void run(String... args) throws Exception {

        System.out.println("Initialized in BootStrap");

        Publisher publisher = new Publisher();
        publisher.setName("Muraya Publishers");
        publisher.setCity("Nairobi");
        publisher.setState("Kenya");

        publisherRepository.save(publisher);

        System.out.println("Publisher count" + publisherRepository.count());

        Author murayaAuthor = new Author("Dan","Muraya");
        Book murayaBook= new Book("Coming to birth","2O342834E23492DU");

        Set<Book> booksSet = new HashSet<>();
        booksSet.add(murayaBook);

        murayaAuthor.setBooks(booksSet);
        murayaBook.getAuthors().add(murayaAuthor);

        murayaBook.setPublisher(publisher);
        publisher.getBooks().add(murayaBook);

        authorRespository.save(murayaAuthor);
        bookRepository.save(murayaBook);
        publisherRepository.save(publisher);

        Author deno = new Author("Deno","Kaimee");
        Book elegante= new Book("Elegant designs","4563FI4F82J7D2");

        Set<Book> anotherBooksSet = new HashSet<>();
        anotherBooksSet.add(elegante);

        deno.setBooks(anotherBooksSet);
        elegante.getAuthors().add(deno);
        publisher.getBooks().add(elegante);

        authorRespository.save(deno);
        bookRepository.save(elegante);
        publisherRepository.save(publisher);

        System.out.println("Number of books" + bookRepository.count());

        System.out.println("Publisher number of books" + publisherRepository.count());

    }
}
