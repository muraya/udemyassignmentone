package com.grefort.udemyassignmentone.repositories;

import com.grefort.udemyassignmentone.model.Book;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BookRepository extends CrudRepository<Book, Long> {
}
