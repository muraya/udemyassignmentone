package com.grefort.udemyassignmentone.repositories;

import com.grefort.udemyassignmentone.model.Publisher;
import org.springframework.data.repository.CrudRepository;

public interface PublisherRepository extends CrudRepository<Publisher, Long> {
}
