package com.grefort.udemyassignmentone.repositories;

import com.grefort.udemyassignmentone.model.Author;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AuthorRespository extends CrudRepository<Author, Long> {
}
