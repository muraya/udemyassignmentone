package com.grefort.udemyassignmentone;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UdemyassignmentoneApplication {

	public static void main(String[] args) {
		SpringApplication.run(UdemyassignmentoneApplication.class, args);
	}

}
